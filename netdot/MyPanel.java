//CSE 223 PA5
//Created by: The Homies; Cyrus Santiago and Griffen Agnello
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	Board board=new Board();
	int xInc=50,yInc=50;
	int margin=10;
	int rowNumber=8;
	int columnNumber=8;
	public void paint(Graphics g) {
			super.paint(g);
			int x=10, y=10;		//Begin at margins of panel
			for (int i=0; i<=rowNumber;i++)	{
				y=10;	//reset y coordinate so it can paint the new column of dots
				for(int j=0; j<=columnNumber; j++)	{
					if(y<getHeight())	{
						g.fillOval(x, y, 5, 5);
						board.acquireRowColumn(rowNumber, columnNumber); //Sends the new row/column number to the board object
						board.buildCell(j, i);						//Uses the current column/row number to make a cell
						y+=board.cellHeight;		//Increment by the specified dimensions
					}
				}
				x+=board.cellWidth;
			}
	}
	
	//Determines which line to paint, needs to check for illegal input and if a box is full
	public boolean paintLine(Graphics g,int x, int y)	{	//x and y are the coordinates of a mouse click
		int r=0,c=0,closest;
		c=board.getCol(x);	//Grabs cell closest to the mouse click
		r=board.getRow(y);
		closest = y - board.Cell[c][r].topLine; //Closest to top line
		if(closest > board.Cell[c][r].bottomLine - y) 	closest = board.Cell[c][r].bottomLine - y;	//Closest to bottom line
		if(closest > x - board.Cell[c][r].leftLine)		closest = x - board.Cell[c][r].leftLine;	//Closest to left line
		if(closest > board.Cell[c][r].rightLine - x) 	closest = board.Cell[c][r].rightLine - x; 	//Closest to right line
		//System.out.println("Topline: "+board.Cell[c][r].topLine+" Bottom: "+board.Cell[c][r].bottomLine);
		if(x>420||y>420) { //If they click in the "no man's land" on the bottom or right sides
			return false;
		}
		if(closest == y - board.Cell[c][r].topLine && c!=8)	{ //When topLine is closest and it's not the 8th column (edge case)
			if(board.Cell[c][r].top==true || c==8) { //If it already exists or is an edge case
				return false;
			}
			board.Cell[c][r].top=true;
			if(r!=0) {
				board.Cell[c][r-1].bottom=true; //If it's not the top row, insure the upper box knows there's a line
			}
			//System.out.println("topLine is closest");
			g.drawLine(board.Cell[c][r].leftLine,board.Cell[c][r].topLine+board.cellHeight,board.Cell[c][r].rightLine,board.Cell[c][r].topLine+board.cellHeight);
			return true;
		}
		if(closest == board.Cell[c][r].bottomLine - y) { //When bottomLine is closest
			if(board.Cell[c][r].bottom==true || c==8) { //If it already exists or is an edge case
				return false;
			}
			board.Cell[c][r].bottom=true;
			if(r+1!=8) {
				board.Cell[c][r+1].top=true; //If it's not bottom row, insure the box below knows there's a line
			}
			//System.out.println("bottomLine is closest"); debugging code
			g.drawLine(board.Cell[c][r].leftLine,board.Cell[c][r].bottomLine+board.cellHeight,board.Cell[c][r].rightLine,board.Cell[c][r].bottomLine+board.cellHeight);
			return true;
		}
		if(closest == x- board.Cell[c][r].leftLine && r!=8 )	{ //When leftLine is closest and it's not the 8th row (edge case)
			if(board.Cell[c][r].left==true) {
				return false;
			}
			board.Cell[c][r].left=true;
			if(c!=0) {
				board.Cell[c-1][r].right=true;
			}
			//System.out.println("leftLine is closest"); debugging code
			g.drawLine(board.Cell[c][r].leftLine,board.Cell[c][r].topLine+board.cellHeight,board.Cell[c][r].leftLine,board.Cell[c][r].bottomLine+board.cellHeight);
			return true;
		}
		if(closest == board.Cell[c][r].rightLine - x && r!=8)	{ //When rightLine is closest and it's not the 8th row (edge case)
			if(board.Cell[c][r].right==true) {
				return false;
			}
			board.Cell[c][r].right=true;
			if(c!=8) {
				board.Cell[c+1][r].left=true;
			}
			//System.out.println("rightLine is closest"); debugging code
			g.drawLine(board.Cell[c][r].rightLine,board.Cell[c][r].topLine+board.cellHeight,board.Cell[c][r].rightLine,board.Cell[c][r].bottomLine+board.cellHeight);
			return true;
		}
		return false;
	}
	public boolean checkFullBox(Graphics g, int c, int r, String name)	{
		if(board.Cell[c][r].top==true && board.Cell[c][r].bottom==true && board.Cell[c][r].left==true && board.Cell[c][r].right==true) {
			if(!board.Cell[c][r].isDrawn)	{//If no initial in box yet
				g.drawString(name,(board.Cell[c][r].leftLine)+(board.cellWidth/2),(board.Cell[c][r].bottomLine+board.cellHeight/2));
				board.Cell[c][r].isDrawn=true;
				return true;
			}	
		}
		return false;
	}
	//Used in Dots Class. Gets new row/column number whenever it changes.
	public void acquireRowColumn(int newRowNumber, int newColumnNumber) {
		rowNumber=newRowNumber;
		columnNumber=newColumnNumber;
	}
	//restarts game and sets everything to false, makes a clean slate
	public void restartGame(Graphics g)	{
		for (int i=0; i<=rowNumber;i++)	{
			for(int j=0; j<=columnNumber;j++)	{
				board.Cell[j][i].left=false;
				board.Cell[j][i].right=false;
				board.Cell[j][i].bottom=false;
				board.Cell[j][i].top=false;
			}
		}
		g.setColor((Color.white));
		g.fillRect(0, 0, 450, 510);
		g.setColor(Color.black);
	}

}
