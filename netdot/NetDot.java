//Made by Cyrus Santiago and Griffen Agnello
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JDesktopPane;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextArea;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.ServerSocket;
import java.net.Socket;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.net.ServerSocket;
import java.net.Socket;
import java.awt.event.MouseMotionAdapter;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class NetDot extends JFrame {

	private JPanel contentPane;
	private MyPanel gamePanel;
	private JPanel bottemPanel;
	private JPanel sidePanel2;
	private JTextField player2NameEntry;
	private JTextField player1NameEntry;
	private JTextArea player2Header;
	private JTextArea player1Header;

	private String player1Name = "Server"; // Default names and score
	private String player2Name = "Client";
	private String name;
	public int player1Score = 0;
	public int player2Score = 0;
	private JButton connectButton;
	private JLabel scoreBoard;
	private boolean score;
	private boolean valid; // determines if move valid
	private int rowNumber = 8;
	private int columnNumber = 8;
	private JButton resetButton;
	int turn = 1; // Determines which player's turn. "1" is PLAYER1. "-1" is PLAYER2. Starts with
																						// player 1
	int playerID = -1; // "1" player is server, "-1" for player is client

	ServerSocket serverSock;
	Socket sock;
	NetThread netThread;
	private JLabel turnLabel;
	private JRadioButton serverRadioButton;
	private JRadioButton clientRadioButton;
	private JTextField serverConnectorEntry;
	public static NetDot frame;
	boolean buttonState=false;

	/**
	 * Launch the application.
	 */
	public static NetDot frame() {
		return frame;
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new NetDot();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NetDot() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 510);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		// Setting up panel layout
		gamePanel = new MyPanel();
		gamePanel.setBounds(50, 10, 10, 10);
		gamePanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.add(gamePanel, BorderLayout.CENTER);

		JPanel sidePanel = new JPanel();
		contentPane.add(sidePanel, BorderLayout.EAST);
		sidePanel.setLayout(new GridLayout(0, 1, 0, 0));
		////////////////////
		sidePanel2 = new JPanel();
		sidePanel.add(sidePanel2);
		GridBagLayout gbl_sidePanel2 = new GridBagLayout();
		gbl_sidePanel2.columnWidths = new int[] { 124, 0 };
		gbl_sidePanel2.rowHeights = new int[] { 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_sidePanel2.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_sidePanel2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		sidePanel2.setLayout(gbl_sidePanel2);

		serverRadioButton = new JRadioButton("Server");
		serverRadioButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				clientRadioButton.setSelected(false);
				serverRadioButton.setSelected(true);
				player2NameEntry.setEditable(false);
				player1NameEntry.setEditable(true);
				connectButton.setEnabled(true);
				connectButton.setText("Start");
			}
		});
		GridBagConstraints gbc_serverRadioButton = new GridBagConstraints();
		gbc_serverRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_serverRadioButton.gridx = 0;
		gbc_serverRadioButton.gridy = 3;
		sidePanel2.add(serverRadioButton, gbc_serverRadioButton);
		/////////////////////
		player1Header = new JTextArea("Player 1:");
		player1Header.setEditable(false);
		GridBagConstraints gbc_player1Header = new GridBagConstraints();
		gbc_player1Header.insets = new Insets(0, 0, 5, 0); // header box above entry box
		gbc_player1Header.fill = GridBagConstraints.BOTH;
		gbc_player1Header.gridx = 0;
		gbc_player1Header.gridy = 4;
		sidePanel2.add(player1Header, gbc_player1Header);
		//////////////////////
		player1NameEntry = new JTextField();
		GridBagConstraints gbc_player1NameEntry = new GridBagConstraints();
		gbc_player1NameEntry.insets = new Insets(0, 0, 5, 0); // Box to enter player 1's name
		gbc_player1NameEntry.fill = GridBagConstraints.HORIZONTAL;
		gbc_player1NameEntry.gridx = 0;
		gbc_player1NameEntry.gridy = 5;
		sidePanel2.add(player1NameEntry, gbc_player1NameEntry);
		player1NameEntry.setColumns(10);
		clientRadioButton = new JRadioButton("Client");
		clientRadioButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				clientRadioButton.setSelected(true);
				serverRadioButton.setSelected(false);
				player1NameEntry.setEditable(false);
				player2NameEntry.setEditable(true);
				connectButton.setEnabled(true);
				connectButton.setText("Connect");
			}
		});
		GridBagConstraints gbc_clientRadioButton = new GridBagConstraints();
		gbc_clientRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_clientRadioButton.gridx = 0;
		gbc_clientRadioButton.gridy = 6;
		sidePanel2.add(clientRadioButton, gbc_clientRadioButton);
		//////////////////////
		player2Header = new JTextArea("Player 2:");
		player2Header.setEditable(false); // Header box above entry box
		GridBagConstraints gbc_player2Header = new GridBagConstraints();
		gbc_player2Header.insets = new Insets(0, 0, 5, 0);
		gbc_player2Header.fill = GridBagConstraints.BOTH;
		gbc_player2Header.gridx = 0;
		gbc_player2Header.gridy = 7;
		sidePanel2.add(player2Header, gbc_player2Header);
		///////////////////////
		player2NameEntry = new JTextField(); // Box to enter Player 2's name
		GridBagConstraints gbc_player2NameEntry = new GridBagConstraints();
		gbc_player2NameEntry.fill = GridBagConstraints.HORIZONTAL;
		gbc_player2NameEntry.insets = new Insets(0, 0, 5, 0);
		gbc_player2NameEntry.anchor = GridBagConstraints.NORTH;
		gbc_player2NameEntry.gridx = 0;
		gbc_player2NameEntry.gridy = 8;
		sidePanel2.add(player2NameEntry, gbc_player2NameEntry);
		player2NameEntry.setColumns(10);

		/////////////////////////
		connectButton = new JButton("Welcome!");
		connectButton.setEnabled(false);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buttonState==false) {
					if (serverRadioButton.isSelected()) {
						playerID = 1;
						establishServerConnection();
					} else {
						if (!establishClientConnection(serverConnectorEntry.getText())) { // If the connection for client
																							// failed, don't proceed any
																							// further.
							turnLabel.setText("Error connecting to " + serverConnectorEntry.getText());
							return;
						}
					}
					buttonState=true;
					prepareGameScreen();
				}
				else if(buttonState==true) {
					gamePanel.restartGame(getGraphics());
					player1Score = 0;
					player2Score = 0;
					setScoreBoard();
					repaint();
					netThread.sendClick(8000, 8000,sock);
					netThread.stopItGetSomeHelp(playerID);
				}
				netThread = new NetThread(sock); //Thread is made
				NetThread netThreadFrom = new NetThread(sock);

				netThread.start();
					netThreadFrom.start();

				gamePanel.addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseMoved(MouseEvent e) {
						if (netThread.scanClickReceived) {
							updateGameBoard(netThread.xcoord, netThread.ycoord);
							setTurnLabel(valid);
							System.out.println("updated game board as the " + playerID);
							netThread.scanClickReceived = false;
						}
					}
				});
				gamePanel.addMouseListener(new MouseAdapter() { // Begins reading mouse clicks on board
					@Override
					public void mouseClicked(MouseEvent e) {
						if(turn != playerID) {
							turnLabel.setText("Wait your turn!");
							return;
						}
						int clickX = e.getX();
						int clickY = e.getY();
						//updateGameBoard(clickX, clickY);
						netThread.sendClick(clickX, clickY,sock);
						updateGameBoard(clickX, clickY);
						setTurnLabel(valid);
					}
				});
			}
		});

		/////////////////////////////
		serverConnectorEntry = new JTextField();
		serverConnectorEntry.setText("localhost");
		GridBagConstraints gbc_serverConnectorEntry = new GridBagConstraints();
		gbc_serverConnectorEntry.insets = new Insets(0, 0, 5, 0);
		gbc_serverConnectorEntry.fill = GridBagConstraints.HORIZONTAL;
		gbc_serverConnectorEntry.gridx = 0;
		gbc_serverConnectorEntry.gridy = 9;
		sidePanel2.add(serverConnectorEntry, gbc_serverConnectorEntry);
		serverConnectorEntry.setColumns(10);
		GridBagConstraints gbc_connectButton = new GridBagConstraints();
		gbc_connectButton.insets = new Insets(0, 0, 5, 0);
		gbc_connectButton.gridx = 0; // Start button size
		gbc_connectButton.gridy = 17;
		sidePanel2.add(connectButton, gbc_connectButton);

		/////////////////////////////
		bottemPanel = new JPanel();
		contentPane.add(bottemPanel, BorderLayout.SOUTH);

		turnLabel = new JLabel("");
		turnLabel.setEnabled(true);
		turnLabel.setVisible(true);
		bottemPanel.add(turnLabel);
		scoreBoard = new JLabel(""); // adds score board box
		bottemPanel.add(scoreBoard);
		
		scoreBoard.setVisible(false); // Score board is initially invisible before game start
		////////////////////////
	}

	// Methods
	private void setScoreBoard() {
		scoreBoard.setText("Scoreboard " + player1Name + ":" + player1Score + "  " + player2Name + ":" + player2Score);
	}

	// This acquires the initial of the specified player. An argument of "1" is
	// accepted to access player 1's name.
	// A "2" is accepted to access player 2's name. The first initial is returned in
	// the form of a string.
	public String getPlayerInitial(int playerNumber) {
		String initialString = "";
		if (playerNumber == 1) {
			initialString = "" + (player1Name.charAt(0));
		} else {
			initialString = "" + (player2Name.charAt(0));
		}
		return (initialString);
	}

	// This acquires the player name during the turn
	public String getPlayer(int playerNumber) {
		String nameString = "";
		if (playerNumber == 1) {
			nameString = "" + (player1Name);
		} else {
			nameString = "" + (player2Name);
		}
		return (nameString);
	}

	public void setTurnLabel(boolean valid) {
		if (valid == true) { // Switches turn if valid input
			turn = -(turn);
			if (turn == 1) {
				name = getPlayerInitial(1);
			} else {
				name = getPlayerInitial(0);
			}
			turnLabel.setText(name + "'s turn");
		} else { // Informs user they've made an invalid action, allows them to pick another line
					// on their turn
			turnLabel.setText("Pick a valid Line!");
		}
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	private void setPlayerNameFields() {
		if ((player1NameEntry.getText()).equals("")) {
			player1NameEntry.setText("Server");
		} // Checks if the name boxes are empty. Fills the name variables if filled.
		else {
			player1Name = player1NameEntry.getText();
		}
		if ((player2NameEntry.getText()).equals("")) {
			player2NameEntry.setText("Client");
		} else {
			player2Name = player2NameEntry.getText();
		}
	}

	private void winner() {
		if (player1Score + player2Score == rowNumber * columnNumber) {
			turnLabel.setText("Game Over!");
			if (player1Score == player2Score) {
				scoreBoard.setText("It's a Tie!");
			} else if (player1Score > player2Score) {
				scoreBoard.setText(player1Name + " Wins!");
			} else {
				scoreBoard.setText(player2Name + " Wins!");
			}
		}
	}

	private void prepareGameScreen() {
		setPlayerNameFields();
		player1NameEntry.setEnabled(false); // Locks in names and number of dots so they can't be changed
		player2NameEntry.setEnabled(false);
		serverRadioButton.setEnabled(false);
		clientRadioButton.setEnabled(false);
		serverConnectorEntry.setEnabled(false);
		connectButton.setText("Quit");
		
		setScoreBoard();
		setTurnLabel(false);
		scoreBoard.setVisible(true); // Shows score board
		//resetButton.setVisible(true);
	}
	
	
	public void updateGameBoard(int x, int y) {
		Board b = new Board();
		int r = b.getRow(y);
		int c = b.getCol(x);
		valid = gamePanel.paintLine(getGraphics(), x, y);
		if (c != 8 || r != 8) { // Edge case for 8th column/row that is rendered on the bottom and right
								// edge
			score = gamePanel.checkFullBox(getGraphics(), c, r, name);
		}
		if (score == true) {
			if (turn==1) {
				player1Score++;
			} else {
				player2Score++;
			}
		}
		setScoreBoard();
		winner();
	}

	private boolean establishServerConnection() {
		try {
			serverSock = new ServerSocket(1234);
		} catch (Exception e) {
			System.out.println("Error creating server socket");
			e.printStackTrace();
			return false;
		}
		try {
			sock = serverSock.accept();
		} catch (Exception e) {
			System.out.println("Error accepting connection");
			e.printStackTrace();
			return false;
		}
		System.out.println("Server has received connection: socket=" + sock);
		return true;
	}

	private boolean establishClientConnection(String hostName) {
		try {
			sock = new Socket(hostName, 1234);
		} catch (Exception e) {
			// e.printStackTrace();
			return false;
		}
		return true;
	}
}
