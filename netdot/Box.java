//CSE 223 PA5
//Created by: The Homies; Cyrus Santiago and Griffen Agnello
//Placeholder for box methods (drawing edges, putting initial, etc.)
public class Box {
	boolean top, bottom, left, right; //True if line is drawn, False if line isn't
	boolean isDrawn;
	int topLine, bottomLine, leftLine, rightLine; //Lines for click mapping
	String playerInitial; //Stores Initial of User who got box
	
	public Box(){
			top=bottom=left=right=false; //Boxes start empty
			isDrawn=false;
			topLine=bottomLine=leftLine=rightLine=0; //Lines start at zero
	}
	
}

