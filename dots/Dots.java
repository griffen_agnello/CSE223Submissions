//Made by Cyrus Santiago and Griffen Agnello
//This is a more updated version than Cyrus'. I tried notifying him but I believe is sleeping and won't see this in time (current time 7:23 AM 5/21)
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JDesktopPane;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextArea;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Dots extends JFrame {

	private JPanel contentPane;
	private MyPanel gamePanel;
	private JPanel bottemPanel;
	private JPanel sidePanel2;
	private JTextField player2NameEntry;
	private JTextField player1NameEntry;
	private JTextArea player2Header;
	private JTextArea player1Header;
	
	private String player1Name="P1"; //Default names and score
	private String player2Name="P2";
	private String name;
	public int player1Score =0;
	public int player2Score =0;
	private JButton startButton;
	private JLabel scoreBoard;
	private boolean score;
	private boolean valid; //determines if move valid
	private JButton moreDotsButton;
	private JButton lessDotsButton;
	private int rowNumber=8;
	private int columnNumber=8;
	private JLabel numberOfDotsLabel;
	private JButton resetButton;
	boolean turn=true;	//Determines which player's turn. TRUE is PLAYER1. FALSE is PLAYER2		Starts with player 1
	private JLabel turnLabel;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblxStronglyRecommended;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 510);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
											//Setting up panel layout
		gamePanel = new MyPanel();
		gamePanel.setBounds(50,10,10,10);
		gamePanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.add(gamePanel, BorderLayout.CENTER);
		
		JPanel sidePanel = new JPanel();
		contentPane.add(sidePanel, BorderLayout.EAST);
		sidePanel.setLayout(new GridLayout(0, 1, 0, 0));
		////////////////////
		sidePanel2 = new JPanel();
		sidePanel.add(sidePanel2);
		GridBagLayout gbl_sidePanel2 = new GridBagLayout();
		gbl_sidePanel2.columnWidths = new int[]{124, 0};
		gbl_sidePanel2.rowHeights = new int[]{19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_sidePanel2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_sidePanel2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		sidePanel2.setLayout(gbl_sidePanel2);
		/////////////////////
		player1Header = new JTextArea("Player 1:");
		player1Header.setEditable(false);
		GridBagConstraints gbc_player1Header = new GridBagConstraints();
		gbc_player1Header.insets = new Insets(0, 0, 5, 0); //header box above entry box
		gbc_player1Header.fill = GridBagConstraints.BOTH;
		gbc_player1Header.gridx = 0;
		gbc_player1Header.gridy = 4;
		sidePanel2.add(player1Header, gbc_player1Header);
		//////////////////////
		player1NameEntry = new JTextField();
		GridBagConstraints gbc_player1NameEntry = new GridBagConstraints();
		gbc_player1NameEntry.insets = new Insets(0, 0, 5, 0); //Box to enter player 1's name
		gbc_player1NameEntry.fill = GridBagConstraints.HORIZONTAL;
		gbc_player1NameEntry.gridx = 0;
		gbc_player1NameEntry.gridy = 5;
		sidePanel2.add(player1NameEntry, gbc_player1NameEntry);
		player1NameEntry.setColumns(10);
		//////////////////////
		player2Header = new JTextArea("Player 2:");
		player2Header.setEditable(false);	//Header box above entry box
		GridBagConstraints gbc_player2Header = new GridBagConstraints();
		gbc_player2Header.insets = new Insets(0, 0, 5, 0);
		gbc_player2Header.fill = GridBagConstraints.BOTH;
		gbc_player2Header.gridx = 0;
		gbc_player2Header.gridy = 6;
		sidePanel2.add(player2Header, gbc_player2Header);
		///////////////////////
		player2NameEntry = new JTextField();	//Box to enter Player 2's name
		GridBagConstraints gbc_player2NameEntry = new GridBagConstraints();
		gbc_player2NameEntry.fill = GridBagConstraints.HORIZONTAL;
		gbc_player2NameEntry.insets = new Insets(0, 0, 5, 0);
		gbc_player2NameEntry.anchor = GridBagConstraints.NORTH;
		gbc_player2NameEntry.gridx = 0;
		gbc_player2NameEntry.gridy = 7;
		sidePanel2.add(player2NameEntry, gbc_player2NameEntry);
		player2NameEntry.setColumns(10);
		/////////////////////
		moreDotsButton = new JButton("More Dots");
		moreDotsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(rowNumber < 15 ) {
					columnNumber++;
					rowNumber++;
					gamePanel.acquireRowColumn(rowNumber, columnNumber);
					numberOfDotsLabel.setText("Current Dot Grid: "+(1+rowNumber)+"x"+(1+columnNumber));
					repaint();
				}
			}	
		});
		
		lblNewLabel = new JLabel("[WARNING]");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 8;
		sidePanel2.add(lblNewLabel, gbc_lblNewLabel);
		GridBagConstraints gbc_moreDotsButton = new GridBagConstraints();
		gbc_moreDotsButton.insets = new Insets(0, 0, 5, 0);
		gbc_moreDotsButton.gridx = 0;
		gbc_moreDotsButton.gridy = 9;
		sidePanel2.add(moreDotsButton, gbc_moreDotsButton);
		
		/////////////////////
		lessDotsButton = new JButton("Less Dots");
		lessDotsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(rowNumber > 1) {
					columnNumber--;
					rowNumber--;
					gamePanel.acquireRowColumn(rowNumber, columnNumber);
					numberOfDotsLabel.setText("Current Dot Grid: "+(rowNumber+1)+"x"+(columnNumber+1));
					repaint();
				}
			}
		});
		GridBagConstraints gbc_lessDotsButton = new GridBagConstraints();
		gbc_lessDotsButton.insets = new Insets(0, 0, 5, 0);
		gbc_lessDotsButton.gridx = 0;
		gbc_lessDotsButton.gridy = 10;
		sidePanel2.add(lessDotsButton, gbc_lessDotsButton);
		
		/////////////////////////
		startButton = new JButton("Start Game");	//Button to start game
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((player1NameEntry.getText()).equals(""))	{
					player1NameEntry.setText("P1");
				} 			//Checks if the name boxes are empty. Fills the name variables if filled.
				else {
					player1Name=player1NameEntry.getText();	}
				if((player2NameEntry.getText()).equals(""))	{
					player2NameEntry.setText("P2");
				}
				else {
					player2Name=player2NameEntry.getText();	}
				player1NameEntry.setEnabled(false);	//Locks in names and number of dots so they can't be changed
				player2NameEntry.setEnabled(false);
				moreDotsButton.setVisible(false);
				lessDotsButton.setVisible(false);
				startButton.setVisible(false);
				sidePanel.setVisible(false);
				gamePanel.addMouseListener(new MouseAdapter() {	//Begins reading mouse clicks on board
					@Override
					public void mouseClicked(MouseEvent e) {
						int clickX = e.getX();
						int clickY = e.getY();
						Board b=new Board();
						//System.out.println("X: "+clickX+" Y:"+clickY);
						int r=b.getRow(clickY);
						int c=b.getCol(clickX);
						valid=gamePanel.paintLine(getGraphics(), clickX, clickY);
						if(c!=8 || r!=8) { //Edge case for 8th column/row that is rendered on the bottom and right edge
							score=gamePanel.checkFullBox(getGraphics(),c,r,name);
						}
						if(score==true) {
							if(turn) {
								player1Score++;
							}
							else{
								player2Score++;
							}
						}
						setScoreBoard();
						setTurnLabel(valid);
					}
				});
				setScoreBoard();
				scoreBoard.setVisible(true); //Shows score board
				resetButton.setVisible(true);
				turnLabel.setVisible(true);
			}
		});
		//Cyrus Here: I opted to leave this feature in since Griffen stayed up until 4 working on it, and I didn't want to just remove it
		//Extra features, even if broken, shouldn't count toward the grade in this case regardless, so it doesn't hurt
		lblNewLabel_1 = new JLabel("*Feature in Beta*");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 11;
		sidePanel2.add(lblNewLabel_1, gbc_lblNewLabel_1);
		//Cyrus Again: I put this warning since it won't work with other grid types, but it doesn't have to for our purposes
		lblxStronglyRecommended = new JLabel("*9x9 Strongly Recommended*");
		GridBagConstraints gbc_lblxStronglyRecommended = new GridBagConstraints();
		gbc_lblxStronglyRecommended.insets = new Insets(0, 0, 5, 0);
		gbc_lblxStronglyRecommended.gridx = 0;
		gbc_lblxStronglyRecommended.gridy = 12;
		sidePanel2.add(lblxStronglyRecommended, gbc_lblxStronglyRecommended);
		
		numberOfDotsLabel = new JLabel("Current Dot Grid: "+(rowNumber+1)+"x"+(1+columnNumber));
		GridBagConstraints gbc_numberOfDotsLabel = new GridBagConstraints();
		gbc_numberOfDotsLabel.insets = new Insets(0, 0, 5, 0);
		gbc_numberOfDotsLabel.gridx = 0;
		gbc_numberOfDotsLabel.gridy = 13;
		sidePanel2.add(numberOfDotsLabel, gbc_numberOfDotsLabel);
		GridBagConstraints gbc_startButton = new GridBagConstraints();
		gbc_startButton.insets = new Insets(0, 0, 5, 0);
		gbc_startButton.gridx = 0;	//Start button size
		gbc_startButton.gridy = 14;
		sidePanel2.add(startButton, gbc_startButton);
		
		//////////////////////
		bottemPanel = new JPanel();
		contentPane.add(bottemPanel, BorderLayout.SOUTH);
		resetButton = new JButton("Restart Game");
		resetButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				gamePanel.restartGame(getGraphics());
				player1Score =0;
				player2Score =0;
				setScoreBoard();
				repaint();
			}
		});
		
		turnLabel = new JLabel("");
		turnLabel.setEnabled(true);
		turnLabel.setVisible(false);
		bottemPanel.add(turnLabel);
		bottemPanel.add(resetButton);
		resetButton.setVisible(false);	
		scoreBoard = new JLabel("");	//adds score board box
		bottemPanel.add(scoreBoard);
		scoreBoard.setVisible(false); //Score board is initially invisible before game start
		////////////////////////
	}
	//Methods
	private void setScoreBoard() {
		scoreBoard.setText("Scoreboard "+ player1Name + ":" + player1Score + "  " + player2Name + ":" + player2Score);
	}
		
	//This acquires the initial of the specified player. An argument of "1" is accepted to access player 1's name.
	//A "2" is accepted to access player 2's name. The first initial is returned in the form of a string.
	public String getPlayerInitial(int playerNumber) {
		String initialString="";
		if(playerNumber==1) {
			initialString=""+(player1Name.charAt(0))+"1";	}				
	else	{
			initialString=""+(player2Name.charAt(0)+"2");	}
			return(initialString);
	}
	//This acquires the player name during the turn
	public String getPlayer(int playerNumber) {
		String nameString="";
		if(playerNumber==1) {
			nameString=""+(player1Name);	}				
	else	{
			nameString=""+(player2Name);	}
			return(nameString);
	}
	
	private void setTurnLabel(boolean valid) {
		if(valid==true) { //Switches turn if valid input
			turn=!turn;
			if(turn) {
				name = getPlayerInitial(1);
			}
			else	{
				name =getPlayerInitial(0);
			}
			turnLabel.setText(name+"'s turn"); 
		}
		else { //Informs user they've made an invalid action, allows them to pick another line on their turn
			turnLabel.setText("Pick a valid Line!");
		}
	}
	
	public int getRowNumber()	{
		return rowNumber;
	}
	
	public int getColumnNumber()	{
		return columnNumber;
	}
}